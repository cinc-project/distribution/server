#!/bin/bash -e
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2020, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TOP_DIR="$(pwd)"
export CI_PROJECT_DIR=${CI_PROJECT_DIR:-${TOP_DIR}}
source /home/omnibus/load-omnibus-toolchain.sh
set -ex
mkdir -p ${TOP_DIR}/cache/git_cache
echo "cache_dir '${TOP_DIR}/cache'" >> chef-server/omnibus/omnibus.rb
echo "git_cache_dir '${TOP_DIR}/cache/git_cache'" >> chef-server/omnibus/omnibus.rb
echo "use_git_caching true" >> chef-server/omnibus/omnibus.rb
echo "build_retries 0" >> chef-server/omnibus/omnibus.rb
cd chef-server/omnibus
# Workaround caching issue
if [ -d "${TOP_DIR}/cache/git_cache/opt/cinc-project" ] ; then
  cd ${TOP_DIR}/cache/git_cache/opt/cinc-project
  git tag -l | grep more-ruby-cleanup-server | xargs -r git tag -d
  cd ${CI_PROJECT_DIR}/chef-server/omnibus
fi
bundle config set --local path ${CI_PROJECT_DIR}/bundle/vendor
bundle install
bundle exec omnibus build cinc-server -l ${OMNIBUS_LOG_LEVEL:-info} --override append_timestamp:false
