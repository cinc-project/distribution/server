title 'Cinc log output'

control 'Cinc log output' do
  impact 1.0

  describe file '/vagrant/logs/reconfigure.log' do
    its('content') { should_not match %r{^((?!#).)*/opt/opscode} }
    its('content') { should_not match %r{^((?!#).)*/etc/opscode} }
    its('content') { should_not match %r{^((?!#).)*/var/opt/opscode} }
    its('content') { should match %r{/opt/cinc-project} }
    its('content') { should match %r{/etc/cinc-project} }
    its('content') { should match %r{/var/opt/cinc-project} }
  end

  describe file '/vagrant/logs/reconfigure-fips.log' do
    its('content') { should_not match %r{^((?!#).)*/opt/opscode} }
    its('content') { should_not match %r{^((?!#).)*/etc/opscode} }
    its('content') { should_not match %r{^((?!#).)*/var/opt/opscode} }
    its('content') { should match %r{/opt/cinc-project} }
    its('content') { should match %r{/etc/cinc-project} }
    its('content') { should match %r{/var/opt/cinc-project} }
  end

  describe file '/vagrant/logs/backup.log' do
    its('content') { should_not match /Chef Infra Server/ }
    its('content') { should_not match /chef_backup/ }
    its('content') { should_not match /chef-backup/ }
    its('content') { should match /Cinc Server/ }
    its('content') { should match /cinc_backup/ }
    its('content') { should match /cinc-backup/ }
    its('content') { should match /Backup Complete/ }
  end

  describe file '/vagrant/logs/restore.log' do
    its('content') { should_not match /Chef Infra Server/ }
    its('content') { should_not match /chef_backup/ }
    its('content') { should_not match /chef-backup/ }
    its('content') { should match /Cinc Server/ }
    its('content') { should match /cinc_backup/ }
    its('content') { should match /cinc-backup/ }
    its('content') { should match /Restoration Completed/ }
  end

  %w(
    /opt/opscode
    /etc/opscode
    /var/opt/opscode
  ).each do |d|
    describe directory d do
      it { should_not exist }
    end
  end

  %w(
    /opt/cinc-project
    /etc/cinc-project
    /var/opt/cinc-project
  ).each do |d|
    describe directory d do
      it { should exist }
    end
  end
end
