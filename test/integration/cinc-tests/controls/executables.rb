title 'Cinc executables'

bin_path = '/opt/cinc-project/bin'

control 'Common executable tests' do
  impact 1.0
  title 'Validate basic functionality on all platforms'
  desc 'Common test to all platforms'

  describe command "#{bin_path}/cinc-client --version" do
    its('exit_status') { should eq 0 }
    its('stdout') { should match /^Cinc Client:/ }
  end

  describe command "#{bin_path}/cinc-solo --version" do
    its('exit_status') { should eq 0 }
    its('stdout') { should match /^Cinc Client:/ }
  end

  describe command "#{bin_path}/cinc-apply --version" do
    its('exit_status') { should eq 0 }
  end

  describe command "#{bin_path}/cinc-shell --version" do
    its('exit_status') { should eq 0 }
  end

  describe command "#{bin_path}/ohai --version" do
    its('exit_status') { should eq 0 }
  end

  describe command "#{bin_path}/chef-client --version" do
    its('exit_status') { should eq 0 }
    its('stderr') { should match /^Redirecting to cinc-client/ }
    its('stdout') { should match /^Cinc Client:/ }
  end

  describe command "#{bin_path}/chef-solo -l info -o \"\"" do
    its('exit_status') { should eq 0 }
    its('stderr') { should match /^Redirecting to cinc-solo/ }
    its('stdout') { should match /Cinc Zero/ }
    its('stdout') { should match /Cinc Client/ }
    its('stdout') { should match /Cinc-client/ }
    its('stdout') { should match %r{/var/cinc} }
    its('stdout') { should_not match /Chef Infra Zero/ }
    its('stdout') { should_not match /Chef Infra Client/ }
    its('stdout') { should_not match /Chef-client/ }
    its('stdout') { should_not match %r{/etc/chef/client.rb} }
    its('stdout') { should_not match %r{/var/chef} }
  end

  describe command "#{bin_path}/cinc-auditor version" do
    its('exit_status') { should eq 0 }
  end

  describe command "#{bin_path}/cinc-auditor detect" do
    its('exit_status') { should eq 0 }
  end

  describe command "#{bin_path}/inspec version" do
    its('exit_status') { should eq 0 }
    its('stderr') { should match /^Redirecting to cinc-auditor/ }
  end

  describe command "cinc-server-ctl help" do
    its('exit_status') { should eq 0 }
    its('stdout') { should_not match /Chef/ }
    its('stdout') { should_not match /Chef Infra Server/ }
    its('stdout') { should match /Cinc Server/ }
  end

  describe command "cinc-server-ctl status" do
    its('exit_status') { should eq 0 }
    its('stdout') { should match /run: bookshelf:/ }
    its('stdout') { should match /run: opensearch:/ }
    its('stdout') { should match /run: nginx:/ }
    its('stdout') { should match /run: oc_bifrost:/ }
    its('stdout') { should match /run: oc_id:/ }
    its('stdout') { should match /run: opscode-erchef:/ }
    its('stdout') { should match /run: postgresql:/ }
    its('stdout') { should match /run: redis_lb:/ }
  end

  describe command "cinc-server-ctl service-list" do
    its('exit_status') { should eq 0 }
    its('stdout') { should match /^bookshelf\*$/ }
    its('stdout') { should match /^opensearch\*$/ }
    its('stdout') { should match /^nginx\*$/ }
    its('stdout') { should match /^oc_bifrost\*$/ }
    its('stdout') { should match /^oc_id\*$/ }
    its('stdout') { should match /^opscode-erchef\*$/ }
    its('stdout') { should match /^postgresql\*$/ }
    its('stdout') { should match /^redis_lb\*$/ }
  end

  describe command "chef-server-ctl version" do
    its('exit_status') { should eq 0 }
    its('stderr') { should match /^Redirecting to cinc-server-ctl/ }
  end
end
