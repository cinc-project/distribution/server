image: cincproject/omnibus-debian

stages:
 - patch
 - package
 - test
 - deploy
 - publish

variables:
  ORIGIN: https://github.com/chef/chef-server.git
  REF: main
  CHANNEL: unstable
  CINC_PRODUCT: cinc-server
  OMNIBUS_FIPS_MODE: "true"

workflow:
  rules:
    # Run if we trigger a pipeline from the web
    - if: $CI_PIPELINE_SOURCE == "web"
    # Run if we trigger a pipeline from another project (i.e. upstream/chef-server)
    - if: $CI_PIPELINE_SOURCE == "pipeline"
    # Run if this is a merge request
    - if: $CI_MERGE_REQUEST_ID

patch:
  stage: patch
  tags:
    - docker-x86_64
  script:
    - ./patch.sh
  artifacts:
    expire_in: 1mo
    paths:
      - chef-server/

include:
  - local: .gitlab/platforms.yml
  - local: .gitlab/package.yml
  - local: .gitlab/test.yml

# Package stage

package:source:
  stage: package
  needs:
    - patch
  tags:
    - docker-x86_64
  script:
    - ./build-source.sh cinc-server chef-server
  artifacts:
    expire_in: 1mo
    paths:
      - source/*

package:amazonlinux-2023:x86_64:
  extends: .amazonlinux-2023:x86_64
  extends: .package:amazonlinux
  image: cincproject/omnibus-amazonlinux:2023
  cache:
    key: amazonlinux-2023-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "2023"

package:centos-8:x86_64:
  extends: .centos-8:x86_64
  extends: .package:centos
  image: cincproject/omnibus-almalinux:8
  cache:
    key: centos-8-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "8"

package:centos-9:x86_64:
  extends: .centos-9:x86_64
  extends: .package:centos
  image: cincproject/omnibus-almalinux:9
  cache:
    key: centos-9-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "9"

package:centos-10:x86_64:
  extends: .centos-10:x86_64
  extends: .package:centos
  image: cincproject/omnibus-almalinux:10-kitten
  allow_failure: true
  cache:
    key: centos-10-x86_64
  tags:
    - docker-x86_64-v3
  variables:
    PLATFORM_VER: "10"

package:debian-11:x86_64:
  extends: .debian-11:x86_64
  extends: .package:debian
  image: cincproject/omnibus-debian:11
  cache:
    key: debian-11-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "11"

package:debian-12:x86_64:
  extends: .debian-12:x86_64
  extends: .package:debian
  image: cincproject/omnibus-debian:12
  cache:
    key: debian-12-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "12"

package:opensuse-15:x86_64:
  extends: .opensuse-15:x86_64
  extends: .package:opensuse
  image: cincproject/omnibus-opensuse:15
  cache:
    key: opensuse-15-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "15"

package:rocky-9:x86_64:
  extends: .rocky-9:x86_64
  extends: .package:rocky
  image: cincproject/omnibus-rockylinux:9
  cache:
    key: rocky-9-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "9"

package:ubuntu-20.04:x86_64:
  extends: .ubuntu-20.04:x86_64
  extends: .package:ubuntu
  image: cincproject/omnibus-ubuntu:20.04
  cache:
    key: ubuntu-20.04-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "20.04"

package:ubuntu-22.04:x86_64:
  extends: .ubuntu-22.04:x86_64
  extends: .package:ubuntu
  image: cincproject/omnibus-ubuntu:22.04
  cache:
    key: ubuntu-22.04-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "22.04"

package:ubuntu-24.04:x86_64:
  extends: .ubuntu-24.04:x86_64
  extends: .package:ubuntu
  image: cincproject/omnibus-ubuntu:24.04
  cache:
    key: ubuntu-24.04-x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "24.04"

# Test stage

test:amazonlinux-2023:x86_64:
  extends: .amazonlinux-2023:x86_64
  extends: .test:amazonlinux
  image: cincproject/vagrant-virtualbox
  allow_failure: true
  needs:
    - package:amazonlinux-2023:x86_64
  tags:
    - docker-virtualbox
  variables:
    PLATFORM_VER: "2023"

test:centos-8:x86_64:
  extends: .centos-8:x86_64
  extends: .test:centos
  image: cincproject/vagrant-virtualbox
  allow_failure: true
  needs:
    - package:centos-8:x86_64
  tags:
    - docker-virtualbox
  variables:
    PLATFORM_VER: "8"

test:centos-9:x86_64:
  extends: .centos-9:x86_64
  extends: .test:centos
  image: cincproject/vagrant-virtualbox
  allow_failure: true
  needs:
    - package:centos-9:x86_64
  tags:
    - docker-virtualbox
  variables:
    PLATFORM_VER: "9"

test:debian-11:x86_64:
  extends: .debian-11:x86_64
  extends: .test:debian
  allow_failure: true
  needs:
    - package:debian-11:x86_64
  tags:
    - virtualbox
  variables:
    PLATFORM_VER: "11"

test:debian-12:x86_64:
  extends: .debian-12:x86_64
  extends: .test:debian
  allow_failure: true
  needs:
    - package:debian-12:x86_64
  tags:
    - virtualbox
  variables:
    PLATFORM_VER: "12"

test:opensuse-15:x86_64:
  extends: .opensuse-15:x86_64
  extends: .test:opensuse
  image: cincproject/vagrant-virtualbox
  allow_failure: true
  needs:
    - package:opensuse-15:x86_64
  tags:
    - docker-virtualbox
  variables:
    PLATFORM_VER: "15"

test:rocky-9:x86_64:
  extends: .rocky-9:x86_64
  extends: .test:rocky
  image: cincproject/vagrant-virtualbox
  allow_failure: true
  needs:
    - package:rocky-9:x86_64
  tags:
    - docker-virtualbox
  variables:
    PLATFORM_VER: "9"

test:ubuntu-20.04:x86_64:
  extends: .ubuntu-20.04:x86_64
  extends: .test:ubuntu
  image: cincproject/vagrant-virtualbox
  allow_failure: true
  needs:
    - package:ubuntu-20.04:x86_64
  tags:
    - docker-virtualbox
  variables:
    PLATFORM_VER: "20.04"

test:ubuntu-22.04:x86_64:
  extends: .ubuntu-22.04:x86_64
  extends: .test:ubuntu
  allow_failure: true
  needs:
    - package:ubuntu-22.04:x86_64
  tags:
    - virtualbox
  variables:
    PLATFORM_VER: "22.04"

test:ubuntu-24.04:x86_64:
  extends: .ubuntu-24.04:x86_64
  extends: .test:ubuntu
  allow_failure: true
  needs:
    - package:ubuntu-24.04:x86_64
  tags:
    - virtualbox
  variables:
    PLATFORM_VER: "24.04"

test:sources:
  image: cincproject/docker-auditor
  stage: test
  needs:
    - package:source
  tags:
    - docker-x86_64
  script:
    - apk add bash outils-sha256
    - cinc-auditor exec test/integration/cinc-sources --no-distinct-exit --reporter cli junit:junit.xml
  artifacts:
    reports:
      junit: junit.xml

# Deploy stage

.ssh-setup:
  before_script:
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "${SSH_KNOWN_HOSTS}" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

deploy:
  allow_failure: false
  stage: deploy
  extends: .ssh-setup
  # Only run if this is triggered from the web
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: manual
    - when: never
  tags:
    - docker-x86_64
  dependencies:
    - package:source
    - package:amazonlinux-2023:x86_64
    - package:centos-8:x86_64
    - package:centos-9:x86_64
    - package:centos-10:x86_64
    - package:debian-11:x86_64
    - package:debian-12:x86_64
    - package:opensuse-15:x86_64
    - package:rocky-9:x86_64
    - package:ubuntu-20.04:x86_64
    - package:ubuntu-22.04:x86_64
    - package:ubuntu-24.04:x86_64
  script:
    - bash deploy.sh

# Publish stage

publish:
  stage: publish
  extends: .ssh-setup
  dependencies: []
  # Only run if this is triggered from the web
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: on_success
    - when: never
  tags:
    - downloads
  script:
    - sudo mkdir -p /data/mirror/files/${CHANNEL}/cinc-server
    - sudo /usr/bin/rsync -avH /data/incoming/files/${CHANNEL}/cinc-server/ /data/mirror/files/${CHANNEL}/cinc-server/
    - sudo -E -u cinc /usr/local/bin/update-cinc-api
    - ssh -q cinc@${MIRROR_HOST} "~/sync-from-master"
